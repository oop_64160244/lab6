package com.than.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldWithdrawSuccess(){
        BookBank book = new BookBank("Than", 1000);
        book.withdraw(500);
        assertEquals(500, book.getBalance(),0.00001);
    }
    @Test
    public void shouldWithdrawOverBalance(){
        BookBank book = new BookBank("Than", 1000);
        book.withdraw(1500);
        assertEquals(1000, book.getBalance(),0.00001);
    }

    @Test
    public void shouldWithdrawWithNegativeNumber(){
        BookBank book = new BookBank("Than",1000);
        book.withdraw(-100);
        assertEquals(1000, book.getBalance(),0.00001);
    }

    @Test
    public void shouldDepositSuccess(){
        BookBank book = new BookBank("Than",1000);
        book.deposit(300);
        assertEquals(1300, book.getBalance(),0.00001);
    }

    @Test
    public void shouldDepositWithNegativeNumber(){
        BookBank book = new BookBank("Than",1000);
        book.deposit(-300);
        assertEquals(1000, book.getBalance(),0.00001);
    }
}


