package com.than.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test 
    public void shouldCreateRobotSuccess(){
        Robot robot = new Robot("Robot",'R',10,11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void shouldCreateRobotSuccess2(){
        Robot robot  = new Robot("Robot",'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }
    @Test
    public void shouldDownOver(){
        Robot robot = new Robot("Robot",'R',0,Robot.MAX_Y);
        assertEquals(false,robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());
    }

    @Test
    public void shouldUpNegative(){
        Robot robot = new Robot("Robot",'R',0,Robot.MIN_Y);
        assertEquals(false,robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());
    }

    @Test
    public void shouldDownSuccess1(){
        Robot robot = new Robot("Robot",'R',0,0);
        assertEquals(true,robot.down());
        assertEquals(1, robot.getY());
    }

    @Test
    public void shouldDownSuccess2(){
        Robot robot = new Robot("Robot",'R',0,3);
        assertEquals(true,robot.down(2));
        assertEquals(5, robot.getY());
    }
    
    @Test
    public void shouldDownFail1(){
        Robot robot = new Robot("Robot",'R',0,19);
        assertEquals(false,robot.down());
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldDownFail2(){
        Robot robot = new Robot("Robot",'R',0,15);
        assertEquals(false,robot.down(5));
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldUpSuccess(){
        Robot robot = new Robot("Robot",'R',0,1);
        assertEquals(true,robot.up(1));
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpSuccess2(){
        Robot robot = new Robot("Robot",'R',0,4);
        assertEquals(true,robot.up(3));
        assertEquals(1, robot.getY());
    }

    @Test
    public void shouldUpFail1(){
        Robot robot = new Robot("Robot",'R',0,1);
        assertEquals(false,robot.up(3));
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpFail2(){
        Robot robot = new Robot("Robot",'R',0,1);
        assertEquals(false,robot.up(2));
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldLeftOver(){
        Robot robot = new Robot("Robot",'R',Robot.MIN_X,0);
        assertEquals(false,robot.left());
        assertEquals(Robot.MIN_X, robot.getX());
    }

    @Test
    public void shouldLeftSuccess1(){
        Robot robot = new Robot("Robot",'R',1,0);
        assertEquals(true,robot.left());
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldLeftSuccess2(){
        Robot robot = new Robot("Robot",'R',5,0);
        assertEquals(true,robot.left(3));
        assertEquals(2, robot.getX());
    }

    @Test
    public void shouldLeftFail1(){
        Robot robot = new Robot("Robot",'R',3,0);
        assertEquals(false,robot.left(4));
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldLeftFail2(){
        Robot robot = new Robot("Robot",'R',5,0);
        assertEquals(false,robot.left(7));
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRightOver(){
        Robot robot = new Robot("Robot",'R',Robot.MAX_X,0);
        assertEquals(false,robot.right());
        assertEquals(Robot.MAX_X, robot.getX());
    }

    @Test
    public void shouldRightSuccess(){
        Robot robot = new Robot("Robot",'R',0,0);
        assertEquals(true,robot.right());
        assertEquals(1, robot.getX());
    }

    @Test
    public void shouldRightSuccess2(){
        Robot robot = new Robot("Robot",'R',1,0);
        assertEquals(true,robot.right(3));
        assertEquals(4, robot.getX());
    }

    @Test
    public void shouldRightFail1(){
        Robot robot = new Robot("Robot",'R',17,0);
        assertEquals(false,robot.right(3));
        assertEquals(19, robot.getX());
    }

    @Test
    public void shouldRightFail2(){
        Robot robot = new Robot("Robot",'R',15,0);
        assertEquals(false,robot.right(6));
        assertEquals(19, robot.getX());
    }
}
