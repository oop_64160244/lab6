package com.than.lab6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank than = new BookBank("Than",10000.0);
        than.print();
        than.withdraw(2000.0);
        than.print();
        than.deposit(5000.0);
        than.print();

        BookBank mother = new BookBank("Mother",50000.0);
        mother.print();
        mother.withdraw(4000.0);
        mother.print();
        than.deposit(4000.0);
        than.print();
    }
}
